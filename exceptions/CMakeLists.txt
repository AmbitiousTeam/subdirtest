file(GLOB EXCEPTION_SOURCES "src/*.cpp")

if (EXCEPTION_SOURCES)
    add_library (EXCEPTIONS ${EXCEPTION_SOURCES})

    target_include_directories (EXCEPTIONS PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
endif()